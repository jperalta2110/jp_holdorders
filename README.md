# JP_HoldOrder Magento 1 Extension

* Author: Jorge Peralta
* Email: jperalta0789@gmail.com

## Overview

This extension allows for automatic holding of orders by shipment country code and weight.

## Features

* Global enable/disable option.
* Multi-select countries that should not be placed on hold.
* Set maximum weight a shipment can have.

## Installation

Available options:

* Directly from github - download the latest release from the release section
* Using Modman with the modman file in the repository

## Module Set Up

* Go to System > Configuration > Sales > Hold Orders

![System Config Tab](screenshots/system-config-tab.png)


* You will see the configurations for Hold Orders

![System Confic Section](screenshots/system-config-section.png)

Available Settings:

* __Enabled__ - Enables hold order logic.
* __Countries Not To Hold__ - Multiselect of country of destination __not__ to hold.
* __Maximum Shipping Weight__ - Maximum weight(lbs) of a package before placed on hold.