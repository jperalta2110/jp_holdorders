<?php
/**
 * @category    JP
 * @package     JP_HoldOrders
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class JP_HoldOrders_Model_Observer
{
    /**
     * Store helper instance.
     *
     * @var JP_HoldOrders_Helper_Data
     */
    private $_jpHelper;

    /**
     * Get Jp_HoldOrders Helper.
     *
     * @return JP_HoldOrders_Helper_Data
     */
    private function _getJpHelper(){
        if(is_null($this->_jpHelper)){
            $this->_jpHelper = Mage::helper('jp_holdorders');
        }
        return $this->_jpHelper;
    }

    /**
     * Set order status to hold for orders that match constraints.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function holdOrdersMatchingConstraints(Varien_Event_Observer $observer){
        if(!$this->_getJpHelper()->isHoldOrdersEnabled()){
            return;
        }

        /** @var $order Mage_Sales_Model_Order*/
        $order = $observer->getEvent()->getOrder();

        if($this->_shouldHoldOrder($order)){
            $order->hold();
            $order->save();
        }

        return;
    }

    /**
     * @param $order Mage_Sales_Model_Order
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    private function _shouldHoldOrder($order){
        $weight = $order->getWeight();
        if($this->_isOverMaxWeight($weight)){
            return true;
        }

        $countryCode = $order->getShippingAddress()->getCountry();
        if($this->_isCountryCodeNotAllowed($countryCode)){
            return true;
        }

        return false;
    }

    /**
     * @param $weight
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    private function _isOverMaxWeight($weight){
        $maxWeight = (int)$this->_getJpHelper()->getMaxWeight();

        if($weight > $maxWeight){
            return true;
        }

        return false;
    }

    /**
     * @param $countryCode
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     */
    private function _isCountryCodeNotAllowed($countryCode){
        $allowedCountries = $this->_getJpHelper()->getAllowedCountryList();

        if(in_array($countryCode, $allowedCountries)){
            return false;
        }

        return true;
    }
}