<?php
/**
 * @category    JP
 * @package     JP_HoldOrders
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class JP_HoldOrders_Helper_Data extends Mage_Core_Helper_Abstract
{
    const JP_HOLDORDERS_SECTION = 'jp';
    const JP_HOLDORDERS_GROUP = 'hold_orders';
    const JP_HOLDORDERS_FIELD_ACTIVE = 'active';
    const JP_HOLDORDERS_FIELD_COUNTRIES = 'countries';
    const JP_HOLDORDERS_FIELD_MAX_WEIGHT = 'max_weight';

    /**
     * Get config data for specified field.
     *
     * @param $field
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getConfigData($field)
    {
        $path = self::JP_HOLDORDERS_SECTION . '/' . self::JP_HOLDORDERS_GROUP . '/' . $field;
        return Mage::getStoreConfig($path, Mage::app()->getStore());
    }

    /**
     * Check if Hold Orders is enabled in settings.
     *
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function isHoldOrdersEnabled()
    {
        return $this->getConfigData(self::JP_HOLDORDERS_FIELD_ACTIVE);
    }

    /**
     * Get country list from settings.
     *
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getAllowedCountryList(){
        $countryList = $this->getConfigData(self::JP_HOLDORDERS_FIELD_COUNTRIES);

        $countryList = explode(',', $countryList);

        return $countryList;
    }

    /**
     * Get max weight from settings.
     *
     * @return mixed
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getMaxWeight(){
        return $this->getConfigData(self::JP_HOLDORDERS_FIELD_MAX_WEIGHT);
    }
}